#!/usr/bin/env raku
constant $adding_mode = True;
my @dict = 'dict.txt'.IO.lines;
my $word = @dict.roll;
my Str $entered_word;
my @known_entered_words;
my $step = 1;
repeat {
	my @line[5] = '.....'.comb;
	$entered_word = prompt "$step| ";
	given $entered_word.chars <=> $word.chars {
		when More {put 'задовге.';next}
		when Less {put 'закоротке.';next}
	}
	if !@dict.grep($entered_word) {
		put 'слова нема в словнику.';
		if $adding_mode {
			if prompt('додати? т/н: ') eq 'т' {
				spurt 'dict.txt',"$entered_word\n",:append;
				@dict.push: $entered_word;
			} else {next}
		}
		next
	}
	if $entered_word eq any @known_entered_words {put 'вже було';next}
	else {@known_entered_words.push: $entered_word}
	my @known_chars;
	for ($entered_word.comb Z $word.comb).kv ->$n,$val {
		if $val[0] eq $val[1] {
			@line[$n] = '+';
			@known_chars.push: $val[1];
		}
	}
	for $entered_word.comb.kv ->$n,$val {
		next if $val eq any @known_chars;
		if $word.contains($val) {@line[$n] = '?'}
	}
	put ' ' x $step.chars + 2 ~ @line.join();
	$step++;
} while $entered_word ne $word;
put '~ вгадано ~';
